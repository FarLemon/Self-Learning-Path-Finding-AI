class Box {
  PVector pos;
  boolean selectedMap = false;
  
  
// Shows the boxes ---------------------------------------------------------------------------------
  void show(String text) {
    if (selectedMap) {
      fill(100, 255, 100);
    } else if (!selectedMap && mouseX > pos.x && mouseX < pos.x + 80 && mouseY > pos.y && mouseY < pos.y + 20) {
      fill(200);
    } else {
      fill(255); 
    }
    stroke(0);
    rect(pos.x, pos.y, 80, 20);
    fill(0);
    textSize(12);
    text(text, pos.x + 10, pos.y + 14);
  }
}
