# Self Learning Path Finding AI


This is a self learning AI that has the main goal to find the fasted / shortest path from start point to end point.

This project was coded in Java using the Processing environment.


## Getting Started


Step 1 : Download the files.
Unfortunately this program has only be exported as a Windows executable, so if you run on Linus or MacOS, be patient for them to be released.

Step 2 : Uncompress the folder with an archiver software. (7zip, WinRAR, etc...)

Step 3 : Open the folder and open the "Self-Learning-Path-Finding-AI-Executable" folder.

Step 4 : Run the .exe file.


### Prerequisites


To run this application, you may be required to have Java installed. If you don't have Java installed, please install Java at www.java.com.


### How to Edit This Project


If you wish to edit this project, you will need Processing.
You can download Processing at https://processing.org/download/.

Step 1 : Seperate the "Self-Learning-Path-Finding-AI-Base" and "Self-Learning-Path-Finding-AI-Example" folders from the main folder.

Step 2 : Open the folder called "Self-Learning-Path-Finding-AI-Base"

Step 3 : Use Processing to open the "Pathfinding_AI.pde" file.
You should see 6 tabs at the top of the Processing Menu.

Step 4 : Explore, Edit, and Share the newly created project(s).
Don't forget to reference the original work!


### How to Add Your Own Maps


Please read previous section, "How to Edit This Project", before trying to add your own map.

Step 1 : Create the map image.

- 1.a : Open your image editting software.

- 1.b : Create a new 1000x563px image with a white background.

- 1.c : Draw walls (with color #092684) and other obsticals to your liking.
Remember to record spawn location and goal location for Step 5.
To add more obsticals and effects, refer to the "How to Add Your Own Effects" section.

- 1.d : Export the image as "map3.jpg", adding 1 to the number as you create more maps.
This name and file type is required for the code to recognize the map.

Step 2 : Move the image(s) into the "maps" folder. (.../Self-Learning-Path-Finding-AI/maps/)

Step 3 : Open the file called "PathFinder_AI.pde" using Processing.
You should see 6 tabs at the top of the Processing Menu.

Step 4 : On tab "Map", line 2, replace the "2" with the number of maps now inside the "maps" folder.

Step 5 : In the same tab, go to line 27.

- 5.a : Here you can use the same format as used for the previous (original) 2 maps.

    if (i == mapNumber) { <br/>
    spawn = new PVector(x, y); <br/>
    goal = new PVector(x, y); <br/>
    }

Step 6 : Now click on the play button on the top left of the Processing window and watch the Dots learn!


### How to Add Your Own Effects


Please read previous section, "How to Edit This Project", before trying to add your own effects.

Adding effects is a creative way for giving the dots more options! Such as a teleporter block, or a speed boosting section.

Remember, this section is for more experienced coders, so don't stress yourself if you don't understand completely.

Step 1 : Go to tab "PathFinding_AI", line 16.

- 1.a : Here you can add colors and name them for what they're for.
Ex.

    color speedBoost = #f9f50d;

Step 2 : Go to tab "Dot", line 58.

- 2.a : Here you can add the effect for whichever color the dot is on.

    if (test_color == portalEntry) { <br/>
    pos = new PVector(portalExit.x, portalExit.y); <br/>
    }

- 2.b : When doing this, you may have to adjust some other code, or add some extra to make the changes work.
Ex.

    tab "PathFinding_AI", line 8 <br/>
    PVector portalExit; <br/>
    tab "Map", line 30 in the maps section <br/>
    portalExit = new PVector(width - 100, height - 50); <br/>

Step 3 : Create a map that includes the newly added feature.
Refere to the "How to Add Your Own Maps" for creating and adding maps.

Step 4 : Test the map using the play button on the top left of the Processing window.


## Built With


* [Processing 3.3.7](https://processing.org/download/) - Program Used To Code
* [Gimp 2.10.4](https://www.gimp.org/) - Program Used To Create Maps


## Authors


* **Joshua Messer** - [FarLemon](https://github.com/FarLemon)


## License


This project is licensed under the GNU GPLv3 License - see the [LICENSE](LICENSE) file for details

