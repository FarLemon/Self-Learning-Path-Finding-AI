class Population {
  Dot[] dots;
  
  float fitnessSum;
  int gen = 1;
  int bestDot = 0;
  int minStep = 1000;
  int previousMinStep = 1000;
  float previousFitness;
  int lastImprovement = 1;
  int currentDot;
  
  Population(int size) {
    dots = new Dot[size];
    for (int i = 0; i < size; i++) {
      dots[i] = new Dot(); 
    }
  }
  
  
// Show all dots -----------------------------------------------------------------------------------
  void show() {
    for (int i = 1; i < dots.length; i++) {
      dots[i].show();
    }
    dots[0].show();
  }
  
  
// Update all dots ---------------------------------------------------------------------------------
  void update() {
    for (int i = 0; i < dots.length; i++) {
      if (dots[i].brain.step > minStep) {
        dots[i].dead = true; 
      } else {
      dots[i].update(); 
      }
    }
  }
  
  
// Calculate fitness of dots -----------------------------------------------------------------------
  void calcFitness() {
    for (int i = 1; i < dots.length; i++) {
      dots[i].calcFitness();
    }
  }
  
// Return whether dot is dead or reached goal -------------
  boolean allDotsDead() {
    for (int i = 1; i < dots.length; i++) {
      if (!dots[i].dead && !dots[i].reachedGoal) {
        return false; 
      }
    }
    return true;
  }
  
  
// Get dot fitness sum -----------------------------------------------------------------------------
  void calcFitnessSum() {
    fitnessSum = 0;
    for (int i = 0; i< dots.length; i++) {
      fitnessSum += dots[i].fitness;
    }
  }
  
// selects dots parent ------------------------------------
  Dot selectParent() {
    float rand = random(fitnessSum);
    float runningSum = 0;
    
    for (int i = 0; i < dots.length; i++) {
      runningSum += dots[i].fitness;
      if (runningSum > rand) {
        currentDot = i;
        return dots[i];
      }
    }
    return null;
  }
  
  
// Get next generation of dots ---------------------------------------------------------------------
  void naturalSelection() {
     Dot[] newDots= new Dot[dots.length];
     setBestDot();
     calcFitnessSum();
   
     for (int i = 0; i < (int)(newDots.length * bestsChildren); i++) {
       newDots[i] = dots[bestDot].createChild();
       newDots[i].isBestsChild = true;
     }
     newDots[0].isBest = true;
     for (int i = (int)(newDots.length * bestsChildren); i < newDots.length; i++) {
       Dot parent = selectParent();
       newDots[i] = parent.createChild();
       if (dots[currentDot] == dots[bestDot]) {
         newDots[i].isBestsChild = true;
       }
     }
     dots = newDots.clone();
     gen++;
  }
  
  
// Mutate child dots -------------------------------------------------------------------------------
  void mutateChildDots() {
    for (int i = 1; i < (dots.length); i++) {
      if (i% 20 == 0) {
        dots[i].brain.mutate(mutationRate*4);
      } else if (i% 5 == 0) {
        dots[i].brain.mutate(mutationRate*2);
      } else {
        dots[i].brain.mutate(mutationRate);
      }
    }
  }
  
  
// Find highest fitness dot ------------------------------------------------------------------------
  void setBestDot() {
    float max = 0;
    int maxIndex = 0;
 
    for (int i = 0; i < dots.length; i++) {
      if (dots[i].fitness > max) {
        max = dots[i].fitness;
        maxIndex = i;
      }
    }
    bestDot = maxIndex;
    
    if (dots[bestDot].reachedGoal) {
      minStep = dots[bestDot].brain.step;
    }
    
//Prints results in console -------------------------------
    float fitnessDifference = dots[bestDot].fitness - previousFitness;
    int minStepDifference = dots[bestDot].brain.step - previousMinStep;
    
    println("Generation:", gen);
    println("Generations Best Dot: #" + bestDot);
    println("Fitness:", String.format("%.10f", dots[bestDot].fitness * 100), "(+" + (String.format("%.10f", fitnessDifference * 100)) + ")");
    println("Steps:", dots[bestDot].brain.step, "(" + minStepDifference + ")");
    if (fitnessDifference == 0) {
      println("Last Improved on Gen", lastImprovement);
    } else {
      lastImprovement = gen;
      println("Improvement in This Generation");
    }
    println("--------------------------------------------------");
    
    previousFitness = dots[bestDot].fitness;
    previousMinStep = dots[bestDot].brain.step;
  }
}
