class Dot {
  PVector pos;
  PVector vel;
  PVector acc;
  Brain brain;
  
  boolean dead = false;
  boolean reachedGoal = false;
  boolean isBest = false;
  boolean isBestsChild = false;
  
  float fitness = 0;
  
  Dot() {
    brain = new Brain(1000);
    pos = new PVector(spawn.x, spawn.y);
    vel = new PVector(0, 0);
    acc = new PVector(0, 0);
  }
  
  
// Draws the dot on the screen ---------------------------------------------------------------------
  void show() {
    if (isBest) {
      fill(0, 255, 0);
      stroke(0);
      ellipse(pos.x, pos.y, 8, 8);
    } else if (isBestsChild) {
      fill(255, 0, 255);
      stroke(0);
      ellipse(pos.x, pos.y, 6, 6);
    } else {
      noFill();
      stroke(100);
      ellipse(pos.x, pos.y, 4, 4);
    }
  }
  
  
// Moves the dots ----------------------------------------------------------------------------------
  void move() {
    if (brain.step < brain.directions.length) {
      acc = brain.directions[brain.step];
      brain.step++;
    } else {
      dead = true; 
    }
      pos.add(vel);
      vel.add(acc);
      vel.limit(5);
  }
  
  
// Updates dot -------------------------------------------------------------------------------------
  void update() {

// Update dependent on color ------------------------------
      color test_color = get((int)pos.x, (int)pos.y);
      if (test_color == wall) {
        dead = true; 
      }
      if (test_color == portalEntry) {
        pos = new PVector(portalExit.x, portalExit.y);
      }
    
// Update dependent on location ---------------------------
    if (!dead && !reachedGoal) {
      move();
      if (pos.x< 2|| pos.y<2 || pos.x>width-2 || pos.y>height -2) {
        dead = true;
      } else if (dist(pos.x, pos.y, goal.x, goal.y) < 5) {
        reachedGoal = true;
      }
    }
  }
  
  
// Calculates fitness ------------------------------------------------------------------------------
  void calcFitness() {
     if (reachedGoal) {
      fitness = 1.0/16.0 + 10000.0/(float)(brain.step * brain.step);
    } else {
      float distanceToGoal = dist(pos.x, pos.y, goal.x, goal.y);
      fitness = 1.0/(distanceToGoal * distanceToGoal * distanceToGoal * distanceToGoal);
    }
  }
  
// Clone dot ----------------------------------------------
  Dot createChild() {
     Dot child = new Dot();
     child.brain = brain.clone();
     return child;
  }
}
