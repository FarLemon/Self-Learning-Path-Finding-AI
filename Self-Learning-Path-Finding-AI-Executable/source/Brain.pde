class Brain {
  PVector[] directions;
  int step = 0;
  
  Brain(int size) {
    directions = new PVector[size];
    randomize();
  }
  
  
// Sets all vectors in random vector directions ----------------------------------------------------
  void randomize() {
    for (int i = 0; i < directions.length; i++) {
      float randomAngle = random(2*PI);
      directions[i] = PVector.fromAngle(randomAngle);
    }
  }
  
// Returns a copy of this brain object --------------------
  Brain clone() {
    Brain clone = new Brain(directions.length);
    for (int i = 0; i < directions.length; i++) {
      clone.directions[i] = directions[i].copy();
    }
    return clone;
  }
  
  
// Mutates the brain -------------------------------------------------------------------------------
  void mutate(float mutationAmount) {
    for (int i = 0; i < directions.length; i++) {
      float rand = random(1);
      if (rand < mutationAmount) {
        float randomAngle = random(2*PI);
        directions[i] = PVector.fromAngle(randomAngle);
      }
    }
  }
}
