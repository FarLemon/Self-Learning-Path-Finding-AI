class Map {
  int numOfMaps = 2;
  Box[] box = new Box[numOfMaps];
  Box mapMenu = new Box();
  Box restartTest = new Box();
  Box mapMenuExit = new Box();
  
  int placeHolder;
  
  
// Set Spawn, and Goal -----------------------------------------------------------------------------
  void mapSetup(int i) {
    
// Map 1 --------------------------------------------------
    if (i == 1) {
      spawn = new PVector(50, 50);
      goal = new PVector(width-50, height-50);
    }
   
// Map 2 --------------------------------------------------
    if (i == 2) {
      spawn = new PVector(50, 50);
      goal = new PVector(width-50, height/2);
    }
    imgMap = loadImage("maps/map"+i+".jpg");
  }
  
  
// Menu for selecting maps -------------------------------------------------------------------------
  void mapSelector() {
    
    PVector tempVector = new PVector(10, 10);
    mapMenu.pos = new PVector(15, height - 35);
    restartTest.pos = new PVector(width - 90, height - 65);
    mapMenuExit.pos = new PVector(width - 90, height - 35);
    
// Makes the map menu -------------------------------------
    if (mousePressed == true) {
      if (mouseX > mapMenu.pos.x && mouseX < mapMenu.pos.x + 80 && mouseY > mapMenu.pos.y && mouseY < mapMenu.pos.y + 20) {
        placeHolder = mapSelector;
        menuOpenned = true;
        background(255);
      } else if (menuOpenned && mouseX > mapMenuExit.pos.x && mouseX < mapMenuExit.pos.x + 80 && mouseY > mapMenuExit.pos.y && mouseY < mapMenuExit.pos.y + 20) {
        menuOpenned = false;
        mapSelector = placeHolder;
      } else if (menuOpenned && mouseX > restartTest.pos.x && mouseX < restartTest.pos.x + 80 && mouseY > restartTest.pos.y && mouseY < restartTest.pos.y + 20) {
        menuOpenned = false; 
        area.restart();
      }
    }
    
// draws menu items ---------------------------------------
    if (!menuOpenned) {
      mapMenu.show("Map Menu");
      fill(255);
      rect(width - 157, 13, 150, 25);
      fill(0);
      textSize(14);
      text("Current Map: Map " + mapSelector, width - 150, 30);
    } else if (menuOpenned) {
      mapMenuExit.show("Exit Menu");
      restartTest.show("Restart");
      for (int i = 0; i < numOfMaps; i++) {
        box[i] = new Box();
        if (i + 1 == mapSelector) {
          box[i].selectedMap = true;  
        }
        box[i].pos = new PVector(tempVector.x, tempVector.y);
        box[i].show("Map " + (i + 1));
        if (tempVector.x > width-90) {
          int y = (int)(tempVector.y);
          tempVector.x = 10;
          tempVector.y += 35;
        } else {
          tempVector.add(100, 0);
        }
      }
      if (mousePressed == true) {
        for (int i = 0; i < numOfMaps; i++) {
          if (mouseX > box[i].pos.x && mouseX < box[i].pos.x + 80 && mouseY > box[i].pos.y && mouseY < box[i].pos.y + 20) {
            mapSelector = i + 1;
          }
        }
      }
    }
  }
  
  
// Restarts the population with the selected map ---------------------------------------------------
  void restart() {
    area.mapSetup(mapSelector);
    test = new Population(2000); 
  }
}
