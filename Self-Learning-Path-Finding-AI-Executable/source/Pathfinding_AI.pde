// Before hand processes ---------------------------------------------------------------------------
Population test;
Map area = new Map();

PImage imgMap;
PVector spawn;
PVector goal;

int mapSelector = 1;
boolean menuOpenned = false;

// Changable Variables ------------------------------------
float mutationRate = 0.01;
float bestsChildren = 0.15;

// Colors to use for obsticals ----------------------------
color wall = #092684;


// Window setup ------------------------------------------------------------------------------------
void setup() {
  size(1000, 563);
  frameRate(120);
  area.restart();
  println("--------------------------------------------------");
}


// Brings items to window --------------------------------------------------------------------------
void draw() {
  background(255);

  if (!menuOpenned) {
// Draws map ----------------------------------------------
    image(imgMap, 0, 0, width, height);
    fill(255, 0, 0);
    stroke(0);
    ellipse(goal.x, goal.y, 10, 10);
    fill(255, 255, 0);
    ellipse(spawn.x, spawn.y, 30, 30);

// Draw and update dots -----------------------------------
    if (test.allDotsDead()) {
      test.calcFitness();
      test.naturalSelection();
      test.mutateChildDots();
    } else {
      test.update();
      test.show();
    }
  }
  
// Draws selection tabs -----------------------------------
  area.mapSelector();
}
