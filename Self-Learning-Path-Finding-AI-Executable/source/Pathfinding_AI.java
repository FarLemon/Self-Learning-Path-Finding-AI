import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Pathfinding_AI extends PApplet {

// Before hand processes ---------------------------------------------------------------------------
Population test;
Map area = new Map();

PImage imgMap;
PVector spawn;
PVector goal;

int mapSelector = 1;
boolean menuOpenned = false;

// Changable Variables ------------------------------------
float mutationRate = 0.01f;
float bestsChildren = 0.15f;

// Colors to use for obsticals ----------------------------
int wall = 0xff092684;


// Window setup ------------------------------------------------------------------------------------
public void setup() {
  
  frameRate(120);
  area.restart();
  println("--------------------------------------------------");
}


// Brings items to window --------------------------------------------------------------------------
public void draw() {
  background(255);

  if (!menuOpenned) {
// Draws map ----------------------------------------------
    image(imgMap, 0, 0, width, height);
    fill(255, 0, 0);
    stroke(0);
    ellipse(goal.x, goal.y, 10, 10);
    fill(255, 255, 0);
    ellipse(spawn.x, spawn.y, 30, 30);

// Draw and update dots -----------------------------------
    if (test.allDotsDead()) {
      test.calcFitness();
      test.naturalSelection();
      test.mutateChildDots();
    } else {
      test.update();
      test.show();
    }
  }
  
// Draws selection tabs -----------------------------------
  area.mapSelector();
}
class Box {
  PVector pos;
  boolean selectedMap = false;
  
  
// Shows the boxes ---------------------------------------------------------------------------------
  public void show(String text) {
    if (selectedMap) {
      fill(100, 255, 100);
    } else if (!selectedMap && mouseX > pos.x && mouseX < pos.x + 80 && mouseY > pos.y && mouseY < pos.y + 20) {
      fill(200);
    } else {
      fill(255); 
    }
    stroke(0);
    rect(pos.x, pos.y, 80, 20);
    fill(0);
    textSize(12);
    text(text, pos.x + 10, pos.y + 14);
  }
}
class Brain {
  PVector[] directions;
  int step = 0;
  
  Brain(int size) {
    directions = new PVector[size];
    randomize();
  }
  
  
// Sets all vectors in random vector directions ----------------------------------------------------
  public void randomize() {
    for (int i = 0; i < directions.length; i++) {
      float randomAngle = random(2*PI);
      directions[i] = PVector.fromAngle(randomAngle);
    }
  }
  
// Returns a copy of this brain object --------------------
  public Brain clone() {
    Brain clone = new Brain(directions.length);
    for (int i = 0; i < directions.length; i++) {
      clone.directions[i] = directions[i].copy();
    }
    return clone;
  }
  
  
// Mutates the brain -------------------------------------------------------------------------------
  public void mutate(float mutationAmount) {
    for (int i = 0; i < directions.length; i++) {
      float rand = random(1);
      if (rand < mutationAmount) {
        float randomAngle = random(2*PI);
        directions[i] = PVector.fromAngle(randomAngle);
      }
    }
  }
}
class Dot {
  PVector pos;
  PVector vel;
  PVector acc;
  Brain brain;
  
  boolean dead = false;
  boolean reachedGoal = false;
  boolean isBest = false;
  boolean isBestsChild = false;
  
  float fitness = 0;
  
  Dot() {
    brain = new Brain(1000);
    pos = new PVector(spawn.x, spawn.y);
    vel = new PVector(0, 0);
    acc = new PVector(0, 0);
  }
  
  
// Draws the dot on the screen ---------------------------------------------------------------------
  public void show() {
    if (isBest) {
      fill(0, 255, 0);
      stroke(0);
      ellipse(pos.x, pos.y, 8, 8);
    } else if (isBestsChild) {
      fill(255, 0, 255);
      stroke(0);
      ellipse(pos.x, pos.y, 6, 6);
    } else {
      noFill();
      stroke(100);
      ellipse(pos.x, pos.y, 4, 4);
    }
  }
  
  
// Moves the dots ----------------------------------------------------------------------------------
  public void move() {
    if (brain.step < brain.directions.length) {
      acc = brain.directions[brain.step];
      brain.step++;
    } else {
      dead = true; 
    }
      pos.add(vel);
      vel.add(acc);
      vel.limit(5);
  }
  
  
// Updates dot -------------------------------------------------------------------------------------
  public void update() {

// Update dependent on color ------------------------------
      int test_color = get((int)pos.x, (int)pos.y);
      if (test_color == wall) {
        dead = true; 
      }
    
// Update dependent on location ---------------------------
    if (!dead && !reachedGoal) {
      move();
      if (pos.x< 2|| pos.y<2 || pos.x>width-2 || pos.y>height -2) {
        dead = true;
      } else if (dist(pos.x, pos.y, goal.x, goal.y) < 5) {
        reachedGoal = true;
      }
    }
  }
  
  
// Calculates fitness ------------------------------------------------------------------------------
  public void calcFitness() {
     if (reachedGoal) {
      fitness = 1.0f/16.0f + 10000.0f/(float)(brain.step * brain.step);
    } else {
      float distanceToGoal = dist(pos.x, pos.y, goal.x, goal.y);
      fitness = 1.0f/(distanceToGoal * distanceToGoal * distanceToGoal * distanceToGoal);
    }
  }
  
// Clone dot ----------------------------------------------
  public Dot createChild() {
     Dot child = new Dot();
     child.brain = brain.clone();
     return child;
  }
}
class Map {
  int numOfMaps = 2;
  Box[] box = new Box[numOfMaps];
  Box mapMenu = new Box();
  Box restartTest = new Box();
  Box mapMenuExit = new Box();
  
  int placeHolder;
  
  
// Set Spawn, and Goal -----------------------------------------------------------------------------
  public void mapSetup(int i) {
    
// Map 1 --------------------------------------------------
    if (i == 1) {
      spawn = new PVector(50, 50);
      goal = new PVector(width-50, height-50);
    }
   
// Map 2 --------------------------------------------------
    if (i == 2) {
      spawn = new PVector(50, 50);
      goal = new PVector(width-50, height/2);
    }
    imgMap = loadImage("maps/map"+i+".jpg");
  }
  
  
// Menu for selecting maps -------------------------------------------------------------------------
  public void mapSelector() {
    
    PVector tempVector = new PVector(10, 10);
    mapMenu.pos = new PVector(15, height - 35);
    restartTest.pos = new PVector(width - 90, height - 65);
    mapMenuExit.pos = new PVector(width - 90, height - 35);
    
// Makes the map menu -------------------------------------
    if (mousePressed == true) {
      if (mouseX > mapMenu.pos.x && mouseX < mapMenu.pos.x + 80 && mouseY > mapMenu.pos.y && mouseY < mapMenu.pos.y + 20) {
        placeHolder = mapSelector;
        menuOpenned = true;
        background(255);
      } else if (menuOpenned && mouseX > mapMenuExit.pos.x && mouseX < mapMenuExit.pos.x + 80 && mouseY > mapMenuExit.pos.y && mouseY < mapMenuExit.pos.y + 20) {
        menuOpenned = false;
        mapSelector = placeHolder;
      } else if (menuOpenned && mouseX > restartTest.pos.x && mouseX < restartTest.pos.x + 80 && mouseY > restartTest.pos.y && mouseY < restartTest.pos.y + 20) {
        menuOpenned = false; 
        area.restart();
      }
    }
    
// draws menu items ---------------------------------------
    if (!menuOpenned) {
      mapMenu.show("Map Menu");
      fill(255);
      rect(width - 157, 13, 150, 25);
      fill(0);
      textSize(14);
      text("Current Map: Map " + mapSelector, width - 150, 30);
    } else if (menuOpenned) {
      mapMenuExit.show("Exit Menu");
      restartTest.show("Restart");
      for (int i = 0; i < numOfMaps; i++) {
        box[i] = new Box();
        if (i + 1 == mapSelector) {
          box[i].selectedMap = true;  
        }
        box[i].pos = new PVector(tempVector.x, tempVector.y);
        box[i].show("Map " + (i + 1));
        if (tempVector.x > width-90) {
          int y = (int)(tempVector.y);
          tempVector.x = 10;
          tempVector.y += 35;
        } else {
          tempVector.add(100, 0);
        }
      }
      if (mousePressed == true) {
        for (int i = 0; i < numOfMaps; i++) {
          if (mouseX > box[i].pos.x && mouseX < box[i].pos.x + 80 && mouseY > box[i].pos.y && mouseY < box[i].pos.y + 20) {
            mapSelector = i + 1;
          }
        }
      }
    }
  }
  
  
// Restarts the population with the selected map ---------------------------------------------------
  public void restart() {
    area.mapSetup(mapSelector);
    test = new Population(2000); 
  }
}
class Population {
  Dot[] dots;
  
  float fitnessSum;
  int gen = 1;
  int bestDot = 0;
  int minStep = 1000;
  int previousMinStep = 1000;
  float previousFitness;
  int lastImprovement = 1;
  int currentDot;
  
  Population(int size) {
    dots = new Dot[size];
    for (int i = 0; i < size; i++) {
      dots[i] = new Dot(); 
    }
  }
  
  
// Show all dots -----------------------------------------------------------------------------------
  public void show() {
    for (int i = 1; i < dots.length; i++) {
      dots[i].show();
    }
    dots[0].show();
  }
  
  
// Update all dots ---------------------------------------------------------------------------------
  public void update() {
    for (int i = 0; i < dots.length; i++) {
      if (dots[i].brain.step > minStep) {
        dots[i].dead = true; 
      } else {
      dots[i].update(); 
      }
    }
  }
  
  
// Calculate fitness of dots -----------------------------------------------------------------------
  public void calcFitness() {
    for (int i = 1; i < dots.length; i++) {
      dots[i].calcFitness();
    }
  }
  
// Return whether dot is dead or reached goal -------------
  public boolean allDotsDead() {
    for (int i = 1; i < dots.length; i++) {
      if (!dots[i].dead && !dots[i].reachedGoal) {
        return false; 
      }
    }
    return true;
  }
  
  
// Get dot fitness sum -----------------------------------------------------------------------------
  public void calcFitnessSum() {
    fitnessSum = 0;
    for (int i = 0; i< dots.length; i++) {
      fitnessSum += dots[i].fitness;
    }
  }
  
// selects dots parent ------------------------------------
  public Dot selectParent() {
    float rand = random(fitnessSum);
    float runningSum = 0;
    
    for (int i = 0; i < dots.length; i++) {
      runningSum += dots[i].fitness;
      if (runningSum > rand) {
        currentDot = i;
        return dots[i];
      }
    }
    return null;
  }
  
  
// Get next generation of dots ---------------------------------------------------------------------
  public void naturalSelection() {
     Dot[] newDots= new Dot[dots.length];
     setBestDot();
     calcFitnessSum();
   
     for (int i = 0; i < (int)(newDots.length * bestsChildren); i++) {
       newDots[i] = dots[bestDot].createChild();
       newDots[i].isBestsChild = true;
     }
     newDots[0].isBest = true;
     for (int i = (int)(newDots.length * bestsChildren); i < newDots.length; i++) {
       Dot parent = selectParent();
       newDots[i] = parent.createChild();
       if (dots[currentDot] == dots[bestDot]) {
         newDots[i].isBestsChild = true;
       }
     }
     dots = newDots.clone();
     gen++;
  }
  
  
// Mutate child dots -------------------------------------------------------------------------------
  public void mutateChildDots() {
    for (int i = 1; i < (dots.length); i++) {
      if (i% 20 == 0) {
        dots[i].brain.mutate(mutationRate*4);
      } else if (i% 5 == 0) {
        dots[i].brain.mutate(mutationRate*2);
      } else {
        dots[i].brain.mutate(mutationRate);
      }
    }
  }
  
  
// Find highest fitness dot ------------------------------------------------------------------------
  public void setBestDot() {
    float max = 0;
    int maxIndex = 0;
 
    for (int i = 0; i < dots.length; i++) {
      if (dots[i].fitness > max) {
        max = dots[i].fitness;
        maxIndex = i;
      }
    }
    bestDot = maxIndex;
    
    if (dots[bestDot].reachedGoal) {
      minStep = dots[bestDot].brain.step;
    }
    
//Prints results in console -------------------------------
    float fitnessDifference = dots[bestDot].fitness - previousFitness;
    int minStepDifference = dots[bestDot].brain.step - previousMinStep;
    
    println("Generation:", gen);
    println("Generations Best Dot: #" + bestDot);
    println("Fitness:", String.format("%.10f", dots[bestDot].fitness * 100), "(+" + (String.format("%.10f", fitnessDifference * 100)) + ")");
    println("Steps:", dots[bestDot].brain.step, "(" + minStepDifference + ")");
    if (fitnessDifference == 0) {
      println("Last Improved on Gen", lastImprovement);
    } else {
      lastImprovement = gen;
      println("Improvement in This Generation");
    }
    println("--------------------------------------------------");
    
    previousFitness = dots[bestDot].fitness;
    previousMinStep = dots[bestDot].brain.step;
  }
}
  public void settings() {  size(1000, 563); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Pathfinding_AI" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
